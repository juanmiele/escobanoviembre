﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Puntos
    {
        private int escoba=0;

        public int Escoba
        {
            get { return escoba; }
            set { escoba = value; }
        }
        private int cantidaddecartas=0;

        public int Cantidaddecartas
        {
            get { return cantidaddecartas; }
            set { cantidaddecartas = value; }
        }
        private int cantidaddeoro=0;

        public int Cantidaddeoro
        {
            get { return cantidaddeoro; }
            set { cantidaddeoro = value; }
        }
 

        private int oro7=0;

        public int Oro7
        {
            get { return oro7; }
            set { oro7 = value; }
        }
        private Single las70;

        public Single Las70
        {
            get { return las70; }
            set { las70 = value; }
        }

        private int Ganalas70 =0;

        public int GanaLas70
        {
            get { return Ganalas70; }
            set { Ganalas70 = value; }
        }

        private int total=0;

        public int Total
        {
            get { return total; }
            set { total = value; }
        }
    }
}
