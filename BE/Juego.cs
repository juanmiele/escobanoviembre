﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Juego
    {
        public Mazo mazo = new Mazo();
        public Mesa mesa = new Mesa();


        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }



        private bool levanta;

        public bool Levanta
        {
            get { return levanta; }
            set { levanta = value; }
        }

        public Turno turno = new Turno();
        public List<BE.Carta> cartas = new List<BE.Carta>();


        public List<BE.Carta> CartaSeleccionadas
        {
            get { return cartas; }
        }
        public List<BE.Jugador> jugadores = new List<BE.Jugador>();

        private int ultimolevantar;

        public int UltimoLevantar
        {
            get { return ultimolevantar; }
            set { ultimolevantar = value; }
        }
    }
}
