﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Jugador
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        private string usuario;

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }


        private Mano mano = new Mano();

        public Mano MANO
        {
            get { return mano; }
            set { mano = value; }
        }

        private Monton monton = new Monton();

        public Monton MONTON
        {
            get { return monton; }
            set { monton = value; }
        }

        private DateTime tiempojuego;

        public DateTime Tiempojuego
        {
            get { return tiempojuego; }
            set { tiempojuego = value; }
        }

        private int min_juego;

        public int Min_juego
        {
            get { return min_juego; }
            set { min_juego = value; }
        }

        private int victorias = 0;

        public int Victorias
        {
            get { return victorias; }
            set { victorias = value; }
        }
        private int empate = 0;

        public int Empate
        {
            get { return empate; }
            set { empate = value; }
        }
        private int derrota = 0;

        public int Derrota
        {
            get { return derrota; }
            set { derrota = value; }
        }

        private int partidasJugadas = 0;

        public int Partidas_Jugadas
        {
            get { return partidasJugadas; }
            set { partidasJugadas = value; }
        }
        private float promedio = 0;

        public float Promedio
        {
            get { return promedio; }
            set { promedio = value; }
        }
        private int numero;

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }
        private BE.Puntos puntos = new BE.Puntos();

        public BE.Puntos Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }
        private BE.Bitacora bitacora = new BE.Bitacora();

        public BE.Bitacora Bitacora
        {
            get { return bitacora; }
            set { bitacora = value; }
        }


    }
}
