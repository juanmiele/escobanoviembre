﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Bitacora
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private int usuario;

        public int Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }
        private DateTime login;

        public DateTime Login
        {
            get { return login; }
            set { login = value; }
        }

        private DateTime logout;

        public DateTime Logout
        {
            get { return logout; }
            set { logout = value; }
        }

        private int partido;

        public int Partido
        {
            get { return partido; }
            set { partido = value; }
        }

        private int fin;

        public int Fin
        {
            get { return fin; }
            set { fin = value; }
        }



    }
}
