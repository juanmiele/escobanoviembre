﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Log
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }
        private int juego;

        public int Juego
        {
            get { return juego; }
            set { juego = value; }
        }
        private int jugador;

        public int Jugador
        {
            get { return jugador; }
            set { jugador = value; }
        }
        private string movimiento;

        public string Movimiento
        {
            get { return movimiento; }
            set { movimiento = value; }
        }
        private int carta;

        public int Carta
        {
            get { return carta; }
            set { carta = value; }
        }
        private int turno;

        public int Turno
        {
            get { return turno; }
            set { turno = value; }
        }
    }
}
