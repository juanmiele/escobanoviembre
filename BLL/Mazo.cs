﻿using BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Mazo
    {
        DAL.MP_carta mp_carta = new DAL.MP_carta();


        public List<BE.Carta> LlenarMazo()
        {
            List<BE.Carta> cartas = new List<BE.Carta>();

            cartas = mp_carta.Listar();
            cartas = this.Mezclar(cartas);
            return cartas;
        }
        public List<BE.Carta> Mezclar(List<BE.Carta> cartas)
        {
            var list = cartas;
            var randomizedList = new List<BE.Carta>();
            var rnd = new Random();
            while (list.Count != 0)
            {
                var index = rnd.Next(0, list.Count);
                randomizedList.Add(list[index]);
                list.RemoveAt(index);
            }

            return randomizedList;
        }

    }
}
