﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Mesa
    {
        public List<BE.Carta> inicio(BE.Mazo mazo)
        //pone 4 cartas sobra la mesa al inicio
        {
            List<BE.Carta> cartas = new List<BE.Carta>();

            for (int m = 0; m < 4; m++)
            {
                mazo.CARTAS[0].Ubicacion = 8;
                mazo.CARTAS[0].Pocision = m;
                cartas.Add(mazo.CARTAS[0]);
                mazo.CARTAS.RemoveAt(0);

            }
            return cartas;
        }
        public List<BE.Carta> Ordenar(List<BE.Carta> manoAux)
        //Ordena las cartas de la mesa
        {
            List<BE.Carta> orden = new List<BE.Carta>();
            int x = 0;
            foreach (BE.Carta carta in manoAux)
            {
                carta.Pocision = x;
                carta.Ubicacion = 8;
                orden.Add(carta);
                x++;
            }
            return orden;
        }
    }
}

