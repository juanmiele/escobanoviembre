﻿
using BE;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Jugador
    {
        DAL.MP_Log mplog = new DAL.MP_Log();

        DAL.MP_Bitacora mP_Bitacora = new DAL.MP_Bitacora();

        public bool RobarDelMazo(BE.Juego juego, BE.Mazo mazo, BE.Jugador jug)
        {
            bool error = false;
            if (mazo.CARTAS.Count() >= 1)
            {
                jug.MANO.CARTAS.Add(mazo.CARTAS[0]);
                mazo.CARTAS.RemoveAt(0);
            }
            else
            {
                error = true;
            }
            jug.MANO.CARTAS = Ordenar(jug.MANO.CARTAS, jug.Numero);

            return error;
        }
        public bool Crearusuario(string nombre, string password)
        {
            bool error = false;
            BE.Jugador usuario = new BE.Jugador();
            usuario.Usuario = nombre;
            usuario.Password = password;

            return error;
        }
        public BE.Juego EscobaDeMano(BE.Juego juego)
        //verifica si tiene Escoba de mano
        {
            int escoba = 0;
            foreach (BE.Carta carta in juego.mesa.CARTAS)
            {
                escoba = escoba + carta.Numero;
            }
            if (escoba == 15)
            {
                juego.jugadores.Last().MONTON.CARTAS = juego.mesa.CARTAS;
                foreach (BE.Carta carta in juego.mesa.CARTAS)
                {
                    //mplog.EscribirLog_PartidoDetalle(juego, jugador, "EscobaDeMano", carta, juego.turno);
                }
                juego.jugadores.Last().MONTON.Escoba = 1;
                juego.mesa.CARTAS.Clear();
            }
            if (escoba == 30)
            {

                juego.jugadores.Last().MONTON.CARTAS = juego.mesa.CARTAS;
                foreach (BE.Carta carta in juego.mesa.CARTAS)
                {
                    //mplog.EscribirLog_PartidoDetalle(juego, jugador, "EscobaDeMano", carta, juego.turno);
                }
                juego.jugadores.Last().MONTON.Escoba = 2;
                juego.mesa.CARTAS.Clear();
            }

            return juego;

        }
        public List<BE.Jugador> JugadorGanador(List<BE.Jugador> jugadores)
        {
            List<BE.Jugador> resultado = new List<BE.Jugador>();
            for (int m = 0; m < jugadores.Count(); m++)
            {
                SumarCartas(jugadores[m]);
            }
            SumarPuntos(jugadores);
            resultado=Ganador(jugadores);
            return resultado;
        }
        public void SumarCartas(BE.Jugador jugador)
        {
            foreach (BE.Carta carta in jugador.MONTON.CARTAS)
            {
                jugador.Puntos.Cantidaddecartas = jugador.Puntos.Cantidaddecartas + 1;
                switch (carta.Numero)
                {
                    case 7:
                        {
                            jugador.Puntos.Las70 = jugador.Puntos.Las70 + 7;
                            break;
                        }
                    case 6:
                        {
                            jugador.Puntos.Las70 = jugador.Puntos.Las70 + 6;
                            break;
                        }
                    case 5:
                        {
                            jugador.Puntos.Las70 = jugador.Puntos.Las70 + 5;
                            break;
                        }
                    case 4:
                        {
                            jugador.Puntos.Las70 = jugador.Puntos.Las70 + 4;
                            break;
                        }
                    case 3:
                        {
                            jugador.Puntos.Las70 = jugador.Puntos.Las70 + 3;
                            break;
                        }
                    case 2:
                        {
                            jugador.Puntos.Las70 = jugador.Puntos.Las70 + 2;
                            break;
                        }
                    case 1:
                        {
                            jugador.Puntos.Las70 = jugador.Puntos.Las70 + 1/2+5;
                            break;
                        }
                }
                if (carta.Palo == 4)
                {
                    jugador.Puntos.Cantidaddeoro = jugador.Puntos.Cantidaddeoro + 1;
                }
                if (carta.Imagen == 47)
                {
                    jugador.Puntos.Oro7 = 1;
                }
            }

            jugador.Puntos.Escoba = jugador.MONTON.Escoba;
        }
 
        public void SumarPuntos(List<BE.Jugador> jugadores)
        {

            BE.Puntos puntos = new Puntos();
            BE.Puntos ganador = new Puntos();
            for (int m = 0; m < jugadores.Count(); m++)
            {
                if (jugadores[m].Puntos.Cantidaddecartas > puntos.Cantidaddecartas)
                {
                    ganador.Cantidaddecartas = m;
                }
                if (jugadores[m].Puntos.Cantidaddeoro > puntos.Cantidaddeoro)
                {
                    ganador.Cantidaddeoro = m;
                }
                if (jugadores[m].Puntos.Las70 > puntos.Las70)
                {
                    ganador.GanaLas70= m;
                }
                jugadores[m].Puntos.Total = puntos.Escoba;
                jugadores[m].Puntos.Total = puntos.Oro7;
            }
            jugadores[puntos.Cantidaddeoro].Puntos.Total++;
            jugadores[puntos.Cantidaddecartas].Puntos.Total++;
            jugadores[puntos.GanaLas70].Puntos.Total++;
        }
        public List<BE.Jugador> Ganador(List<BE.Jugador> jugadores)
        {
            List<BE.Jugador> ganadores = new List<BE.Jugador>();
            int ganador = 0;
            //mayor cantidad de puntos
            for (int m = 0; m < jugadores.Count(); m++)
            {
                if (jugadores[m].Puntos.Total > ganador)
                {
                    ganador = jugadores[m].Puntos.Total;
                }
            }
            //me quedo con los que tienen mayor cantidad de puntos
            for (int m = 0; m < jugadores.Count(); m++)
            {
                if (jugadores[m].Puntos.Total == ganador)
                {
                    ganadores.Add(jugadores[m]);
                }
                else
                {
                    //guardar registro de perdedor
                    jugadores[m].Bitacora.Fin = 0;
                    Logoff(jugadores[m]);
                }
            }
            //guardar registro de ganadores
            if (ganadores.Count==1)
            {
                jugadores[0].Bitacora.Fin = 3;
                Logoff(jugadores[0]);
            }
            else
            {
                //guardar registro de empate
                for (int e = 0; e < ganadores.Count(); e++)
                {
                    ganadores[e].Bitacora.Fin = 1;
                    Logoff(ganadores[e]);
                }
            }
            
            return ganadores;
        }


        public List<BE.Carta> Inicio(BE.Juego juego, BE.Jugador jugador)
        {
            //Reparte cartas al jugador le da 3
            List<BE.Carta> cartas = new List<BE.Carta>();

            for (int m = 0; m < 3; m++)
            {
                juego.mazo.CARTAS[0].Pocision = m;
                juego.mazo.CARTAS[0].Ubicacion = jugador.Numero;
                //mplog.EscribirLog_PartidoDetalle(juego, jugador, "Reparte", juego.mazo.CARTAS[0], juego.turno);
                cartas.Add(juego.mazo.CARTAS[0]);
                juego.mazo.CARTAS.RemoveAt(0);
            }
            return cartas;
        }



        public List<BE.Carta> Ordenar(List<BE.Carta> manoAux, int pocision)
        //Ordena cartas de la mano

        {
            List<BE.Carta> orden = new List<BE.Carta>();
            int x = 0;

            foreach (BE.Carta carta in manoAux)
            {
                carta.Ubicacion = pocision;
                carta.Pocision = x;
                orden.Add(carta);
                x++;
            }
            return orden;
        }
        public List<BE.Jugador> Detalle(BE.Jugador jugador)
        {
            List<BE.Bitacora> bitacoras = new List<BE.Bitacora>();
            bitacoras = EstadisticasTodas();
            foreach (BE.Bitacora registro in bitacoras)
            {
                if (registro.Usuario == jugador.ID)
                {
                    switch (registro.Fin)
                    {

                        case 0:
                            {
                                jugador.Derrota++;
                                    break;
                            }
                        case 1:
                            {
                                jugador.Empate++;
                                break;
                            }
                        case 3:
                            {
                                jugador.Victorias++;
                                break;
                            }
                    }
                    jugador.Partidas_Jugadas++;
                }
            }
            //jugador=EstadisticasV(jugador);
            //jugador = EstadisticasP(jugador);
            jugador = Tiempo(jugador);
            jugador.Promedio = (float)jugador.Victorias / jugador.Partidas_Jugadas;
            List<BE.Jugador> jugadores = new List<BE.Jugador>();
            jugadores.Add(jugador);
            
            return jugadores;
        }
     
        DAL.MP_jugador mp = new DAL.MP_jugador();
        DAL.MP_Bitacora mpbitacora = new DAL.MP_Bitacora();
        public void Insertar(string nombre, string password)
        {
            mp.Insertar(nombre, password);
        }
        public BE.Jugador Autentificar(string nombre, string password)
        {
            return mp.Autenticar(nombre, password);
        }
        public void Logoff (BE.Jugador jugador)
        {
            mpbitacora.GrabarBitacoraLogin(jugador);
        }
        public BE.Jugador EstadisticasV(BE.Jugador jugador)
        {
            return mp.EstadisticasV(jugador);
        }
        public BE.Jugador Tiempo(BE.Jugador jugador)
        {
            return mp.Tiempo(jugador);
        }
            public BE.Jugador EstadisticasP(BE.Jugador jugador)
        {
            return mp.EstadisticasP(jugador);
        }
        public List<BE.Bitacora> EstadisticasTodas()
        {
            return mpbitacora.ListarResultados();
        }
    }
}
