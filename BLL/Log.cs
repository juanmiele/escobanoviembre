﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Log
    {
        DAL.MP_Log mp = new DAL.MP_Log();
        public int UltimoJuego()
        {
            int id = mp.LeerUltimoJuego();
            return id;
        }
        public void EscribirLogPartido(BE.Juego juego, BE.Jugador jugador, string estado)
        {
            mp.EscribirLog_Partido(juego, jugador, estado);
        }
        public void EscribirLogPartidoDetalle(BE.Juego juego, BE.Jugador jugador, string estado, BE.Carta carta, BE.Turno turno)
        {
            mp.EscribirLog_PartidoDetalle(juego, jugador, estado, carta,turno);
        }
    }
}
