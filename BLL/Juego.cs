﻿using BE;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;



namespace BLL
{
    public class Juego
    {
        Mazo Gestormazo = new Mazo();
        Mesa Gestomeza = new Mesa();
        Turno Gestorturno = new Turno();
        Jugador Gestorjugador = new Jugador();
        Log GestorLog = new Log();
        public BE.Juego juego = new BE.Juego();
        BE.Log log = new BE.Log();
        List<BE.Jugador> JugadorGanador = new List<BE.Jugador>();
        int resultado= 0;
        public void Iniciarjuego(List<BE.Jugador> jugadores, int CantidadDeJugadores)
        {
            //Se carga la id de la partida
            juego.Id = GestorLog.UltimoJuego();
            ////inicia los turnos
            //juego.turno.INDICE = 0;
            //cargar jugadores al juego
            int auxiliar=1;
            for (int m = 0; m < CantidadDeJugadores; m++)
            {
                BE.Jugador jugador = new BE.Jugador();
                jugador = jugadores[m];
                //jugador.Numero = m;
                jugador.Numero = auxiliar;
                auxiliar++;
                jugador.Bitacora.Partido = juego.Id;
                //GestorLog.EscribirLogPartido(juego, jugador, "inicio");
                juego.jugadores.Add(jugador);
                juego.turno.JUGADORES.Add(jugador);
            }
            RepartirInicio();
        }
        private void RepartirInicio()
        {
            //llena el mazo
            juego.mazo.CARTAS = Gestormazo.LlenarMazo();
            //reparte las carta a los jugadores y genera los jugadores
            for (int j = 0; j < juego.jugadores.Count; j++)
            {
                for (int c = 0; c < 3; c++)
                {
                    juego.mazo.CARTAS[0].Pocision = c;
                    juego.mazo.CARTAS[0].Ubicacion = juego.jugadores[j].Numero;
                    juego.jugadores[j].MANO.CARTAS.Add(juego.mazo.CARTAS[0]);
                    juego.mazo.CARTAS.RemoveAt(0);

                }
                juego.jugadores[j].MANO.CARTAS = Gestorjugador.Ordenar(juego.jugadores[j].MANO.CARTAS, juego.jugadores[j].Numero);
            }

            //pone las 4 cartas en la mesa
            juego.mesa.CARTAS=Gestomeza.inicio(juego.mazo);
            //verifica q no alla Escoba de mano
            juego = Gestorjugador.EscobaDeMano(juego);
        }

        

        private void Sumar()
        {
            //valida si suma 15 y levanta
            int total = 0;
            //suma las carta seleccionadas
            foreach (BE.Carta carta in juego.CartaSeleccionadas)
            {
                total = total + carta.Numero;
            }
            // consulta si suma 15
            if (total == 15)
            {
                // si suma 15 saca las cartas de la mesa y de la mano y  las envia al monton del jugador
                juego.Levanta = true;
                foreach (BE.Carta carta in juego.CartaSeleccionadas)
                {
                    juego.mesa.CARTAS.Remove(carta);
                    juego.jugadores[juego.turno.INDICE].MANO.CARTAS.Remove(carta);
                    juego.jugadores[juego.turno.INDICE].MONTON.CARTAS.Add(carta);
                    //verifica si fue una escoba
                    if (juego.mesa.CARTAS.Count == 0)
                    {
                        juego.jugadores[juego.turno.INDICE].MONTON.Escoba++;
                    }
                }
                //y marca que jugador levanto ultimo
                juego.UltimoLevantar = juego.turno.INDICE;

            }
            else
            {
                // si no suma 15
                juego.Levanta = false;

                // tira la carta seleccionada a la mesa
                foreach (BE.Carta carta in juego.CartaSeleccionadas)
                {
                    if (carta.Ubicacion == juego.jugadores[juego.turno.INDICE].Numero)
                    {
                        juego.jugadores[juego.turno.INDICE].MANO.CARTAS.Remove(carta);
                        juego.mesa.CARTAS.Add(carta);
                    }
                }

            }                        
            juego.mesa.CARTAS = Gestomeza.Ordenar(juego.mesa.CARTAS);

        }
        public void Jugar()
        {
            if (SoloUna() == true)
            {
                Sumar();
                FinDeturno();
            }
            else
            {
                //juego.CartaSeleccionadas.Clear();
            }
            if (UltimaCarta() == true)
            {
                FinDelJuego();
            }
            juego.CartaSeleccionadas.Clear();

        }
        private void FinDeturno()
        {
            juego.jugadores[juego.turno.INDICE].MANO.CARTAS = Gestorjugador.Ordenar(juego.jugadores[juego.turno.INDICE].MANO.CARTAS, juego.jugadores[juego.turno.INDICE].Numero);
            Gestorturno.SiguienteTurno(juego.turno);
            //si ya se dio toda la vuelta se da 3 carta a todos los jugadores
            if (juego.mazo.CARTAS.Count > 0)
            {
                if (juego.turno.INDICE == 0)
                {
                    if (juego.jugadores[0].MANO.CARTAS.Count == 0)
                    {

                        for (int m = 0; m < juego.jugadores.Count(); m++)
                        {
                            juego.jugadores[m].MANO.CARTAS = Gestorjugador.Inicio(juego, juego.jugadores[m]);
                        }
                    }
                }
            }
            // se ordenan las cartas de la mesa
            juego.mesa.CARTAS = Gestomeza.Ordenar(juego.mesa.CARTAS);
        }
        public void SeleccionarCarta(BE.Carta cartasmarcadas, bool valida)
        //cartas seleccionadas
        {

            if (valida == true)
            {
                juego.CartaSeleccionadas.Add(cartasmarcadas);
            }
            else
            {
                juego.CartaSeleccionadas.Remove(cartasmarcadas);
            }
        }
        private bool SoloUna()
        {
            int SoloUna = 0;
            bool una = true;
            foreach (BE.Carta carta in juego.CartaSeleccionadas)
            {
                if (carta.Ubicacion == juego.jugadores[juego.turno.INDICE].Numero)
                {
                    SoloUna++;
                }
            }
            if (SoloUna > 1)
            {
                una = false;
            }
            if (SoloUna == 0)
            {
                una = false;
            }
            return una;
        }
        private bool UltimaCarta()
        {
            bool FinDelJuego = false;
            if (juego.mazo.CARTAS.Count == 0)
            {
                if (juego.jugadores.Last().MANO.CARTAS.Count == 0)
                {
                    FinDelJuego = true;
                }
            }
            return FinDelJuego;
        }
        public List<BE.Jugador> Abandonar(BE.Jugador jugador)
        {
            
            juego.jugadores.Remove(jugador);
            jugador.Bitacora.Fin = 0;
            Gestorjugador.Logoff(jugador);
            if (juego.jugadores.Count == 1)
            {

                juego.jugadores[0].Bitacora.Fin = 3;
                Gestorjugador.Logoff(juego.jugadores[0]);
            }
            else
            {
                for (int m = 0; m < juego.jugadores.Count; m++)
                {
                    juego.jugadores[m].Bitacora.Fin = 1;
                    Gestorjugador.Logoff(juego.jugadores[m]);
                }
            }
            JugadorGanador = juego.jugadores;
            return JugadorGanador;
        }
        private void FinDelJuego()
        {
            JugadorGanador = Gestorjugador.JugadorGanador(juego.jugadores);
            resultado = JugadorGanador.Count;
        }
        public List<BE.Carta> CartasMesa()
        {
            return juego.mesa.CARTAS;
        }
        public void cartaSeleccionadaslimpiar()
        {
            juego.CartaSeleccionadas.Clear();
        }
        public List<BE.Carta> CartasJugador()
        {
            return juego.jugadores[juego.turno.INDICE].MANO.CARTAS;
        }
        public List<BE.Carta> CartasParaMostrar()
        {
            List<BE.Carta> cartasparamostrar = new List<BE.Carta>();
            cartasparamostrar.Clear();
            cartasparamostrar = juego.mazo.CARTAS;
            cartasparamostrar.AddRange(juego.jugadores[juego.turno.INDICE].MANO.CARTAS);
            cartasparamostrar.AddRange(juego.mesa.CARTAS);

            return cartasparamostrar;
        }
        public List<BE.Jugador> MuestraGanador()
        {       
            return JugadorGanador;
        }

        public BE.Jugador MuestraJugadorActual()
        {
            return juego.jugadores[juego.turno.INDICE];
        }
        public int Resultado()
        {
            return resultado;
        }
        public List<BE.Jugador> Ganadores()
        {
            return JugadorGanador;
        }
    }

}