﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BE;
using Presentacion;

namespace Presentacion
{
    public partial class micarta : UserControl
    {
        public CartasSeleccionadas cartasSeleccionadas = new CartasSeleccionadas();
        public List<BE.Carta> cartasmarcadas = new List<BE.Carta>();

        public delegate void CartaSeleccionada(micarta Micarta);

        public event CartaSeleccionada CartaClick;
        public micarta()
        {
            InitializeComponent();
        }

        public micarta(Carta carta)
        {

            this.Carta = carta;
            int x = 1;
            int y = 1;
            switch (carta.Ubicacion)
            {

                case 8:
                    {
                        if (carta.Pocision < 9)
                        {
                            y = 350;
                            break;
                        }
                        else
                        {
                            y = 650;
                            carta.Pocision = carta.Pocision - 9;
                            break;
                        }
                    }
                case 0:
                    {
                        y = 650;
                        break;
                    }
            }
            x = carta.Pocision * 220;
            this.Location = new Point(x, y);
            this.CartaClick += Carta_CartaClick;
            InitializeComponent();
            Establecerimagen();
        }


        private int ubicacion;
        public int Ubicacion
        {
            get { return ubicacion; }
            set { ubicacion = value; }
        }

        private Carta carta =new Carta();
        public Carta Carta
        {
            get { return carta; }
            set { carta = value; }
        }
        private bool seleccionada;
        public bool Seleccionada
        {
            get { return seleccionada; }
            set { seleccionada = value; ; }
        }
        private bool levanta;
        public bool Levanta
        {
            get { return levanta; }
            set { levanta = value; ; }
        }
        private void Micarta_Load(object sender, EventArgs e)
        {
            Establecerimagen();
        }
        public void Carta_CartaClick(micarta micarta)
        {
            //if (this.seleccionada == true & this.carta.Ubicacion > 0)

            //if (this.carta.Ubicacion == 0)
            //{
            //    this.seleccionada = false;
            //    this.BackColor = default;
            //}
            //else
            //{
                if (this.seleccionada == false)
                {
                    this.BackColor = Color.Green;
                    this.seleccionada = true;
                    cartasmarcadas.Add(this.carta);

                }
                else
                {
                    this.seleccionada = false;
                    this.BackColor = default;
                    cartasmarcadas.Remove(this.carta);
                }
            //}
        }
        public void PictureBox1_Click(object sender, EventArgs e)
        {
            this.CartaClick(this);
            
        }

        private void Establecerimagen()
        {
            if (carta == null)
            {
                //TODO: Si la carta es nula arropjar alerta al usuario
                return;
            }
            switch (this.carta.Imagen)
            {
                case 11:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Basto\\1.jpg");
                        break;
                    }
                case 12:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Basto\\2.jpg");
                        break;
                    }
                case 13:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Basto\\3.jpg");
                        break;
                    }
                case 14:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Basto\\4.jpg");
                        break;
                    }
                case 15:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Basto\\5.jpg");
                        break;
                    }
                case 16:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Basto\\6.jpg");
                        break;
                    }
                case 17:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Basto\\7.jpg");
                        break;
                    }
                case 18:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Basto\\10.jpg");
                        break;
                    }
                case 19:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Basto\\11.jpg");
                        break;
                    }
                case 110:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Basto\\12.jpg");
                        break;
                    }
                case 21:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Copa\\1.jpg");
                        break;
                    }
                case 22:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Copa\\2.jpg");
                        break;
                    }
                case 23:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Copa\\3.jpg");
                        break;
                    }
                case 24:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Copa\\4.jpg");
                        break;
                    }
                case 25:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Copa\\5.jpg");
                        break;
                    }
                case 26:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Copa\\6.jpg");
                        break;
                    }
                case 27:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Copa\\7.jpg");
                        break;
                    }
                case 28:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Copa\\10.jpg");
                        break;
                    }
                case 29:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Copa\\11.jpg");
                        break;
                    }
                case 210:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Copa\\12.jpg");
                        break;
                    }
                case 31:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Espada\\1.jpg");
                        break;
                    }
                case 32:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Espada\\2.jpg");
                        break;
                    }
                case 33:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Espada\\3.jpg");
                        break;
                    }
                case 34:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Espada\\4.jpg");
                        break;
                    }
                case 35:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Espada\\5.jpg");
                        break;
                    }
                case 36:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Espada\\6.jpg");
                        break;
                    }
                case 37:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Espada\\7.jpg");
                        break;
                    }
                case 38:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Espada\\10.jpg");
                        break;
                    }
                case 39:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Espada\\11.jpg");
                        break;
                    }
                case 310:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Espada\\12.jpg");
                        break;
                    }
                case 41:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Oro\\1.jpg");
                        break;
                    }
                case 42:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Oro\\2.jpg");
                        break;
                    }
                case 43:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Oro\\3.jpg");
                        break;
                    }
                case 44:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Oro\\4.jpg");
                        break;
                    }
                case 45:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Oro\\5.jpg");
                        break;
                    }
                case 46:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Oro\\6.jpg");
                        break;
                    }
                case 47:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Oro\\7.jpg");
                        break;
                    }
                case 48:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Oro\\10.jpg");
                        break;
                    }
                case 49:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Oro\\11.jpg");
                        break;
                    }
                case 410:
                    {
                       pictureBox1.Image = Image.FromFile(".\\Imagenes\\Oro\\12.jpg");
                        break;
                    }
            }
            if (this.carta.Ubicacion==0)
            {
                pictureBox1.Image = Image.FromFile(".\\Imagenes\\back.jpg");
            }
        }
    }
}
