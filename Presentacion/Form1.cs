﻿using BE;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Inicio : Form
    {
        BLL.Jugador gestorjugadores = new BLL.Jugador();
        BLL.Juego gestorjuego = new BLL.Juego();
        List<BE.Jugador> jugadores = new List<BE.Jugador>();
        int CantidadDeJugadores = 0;
        BE.Jugador jugador = new BE.Jugador();





        public Inicio()
        {
            InitializeComponent();
        }
        private void CargarAutomatico()
        {
            CantidadDeJugadores = 2;
            BE.Jugador j1 = new BE.Jugador();
            BE.Jugador j2 = new BE.Jugador();
            string usuario = "juan";
            string password = "eterna";


            j1 = (gestorjugadores.Autentificar(usuario, password));
            jugadores.Add(j1);

            usuario = "cuchu";
            password = "mia";
            j2 = (gestorjugadores.Autentificar(usuario, password));
            jugadores.Add(j2);
            button3.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ValidarUsuario();
        }
        private void limpiar()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            if (this.radioButton1.Checked == true)
            {
                if (this.listBox1.Items.Count == 2)
                {
                    MessageBox.Show(
                    "Ya estan listo todos los jugadores ", "Liso!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CantidadDeJugadores = 2;
                    button3.Visible = true;

                }
            }
            if (this.radioButton2.Checked == true)
            {
                if (this.listBox1.Items.Count == 3)
                {
                    MessageBox.Show(
                    "Ya estan listo todos los jugadores ", "Liso!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CantidadDeJugadores = 3;
                    button3.Visible = true;

                }
            }
            if (this.radioButton3.Checked == true)
            {
                if (this.listBox1.Items.Count == 4)
                {
                    MessageBox.Show(
                    "Ya estan listo todos los jugadores ", "Liso!!!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CantidadDeJugadores = 4;
                    button3.Visible = true;
                }
            }
        }
        private void ActualizarLista()
        {
            listBox1.Items.Clear();

            foreach (BE.Jugador j in jugadores)
            {
                listBox1.Items.Add(j.Usuario);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            GenerarUsuario();
        }

        private void textBox2_TextChanged_1(object sender, EventArgs e)
        {
            textBox2.PasswordChar = System.Convert.ToChar("*");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            gestorjuego.Iniciarjuego(jugadores, CantidadDeJugadores);
            MostrarCartas1();
        }


        private void button4_Click(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            if (listBox1.SelectedIndex > -1)
            {
                gestorjugadores.Logoff(jugadores[index]);
                jugadores.RemoveAt(index);
                ActualizarLista();
            }
        }


        private void MostrarCartas1()
        {
            //borra todo lo que esta en la mesa y vuelve a cargar
            this.Controls.Clear();
            this.Controls.Add(button9);
            this.Controls.Add(button10);
            this.Controls.Add(label6);
            this.Controls.Add(label7);
            this.Controls.Add(label8);
            this.Controls.Add(label9);
            this.Controls.Add(label10);
            this.Controls.Add(label11);
            this.Controls.Add(label12);
            this.Controls.Add(label13);
            button9.Visible = true;
            button10.Visible = true;
            Mostrarcartas(gestorjuego.CartasMesa());
            Mostrarcartas(gestorjuego.CartasJugador());

            //Mostrarcartas(gestorjuego.CartasParaMostrar());
            MostrarPuntos();
        }
        public void Mostrarcartas(List<Carta> cartas)
        {
            // pone en la mesa las cartas
            foreach (Carta carta in cartas)
            {
                micarta mostrarcarta = new micarta(carta);
                mostrarcarta.CartaClick += Carta_CartaClick;
                this.Controls.Add(mostrarcarta);
            }
        }
        public void MostrarPuntos()
        {

            jugador = gestorjuego.MuestraJugadorActual();
            // Muestra puntos
            label6.Text = "Numero:";
            label8.Text = jugador.Numero.ToString();
            label7.Text = "Jugador:";
            label9.Text = jugador.Usuario;
            label10.Text = "Cartas Juntadas:";
            label11.Text = jugador.MONTON.CARTAS.Count.ToString();
            label12.Text = "Escobas:";
            label13.Text = jugador.MONTON.Escoba.ToString();
        }
        private void Carta_CartaClick(micarta seleccioncarta)
        {
            //selecciona cartas
            gestorjuego.SeleccionarCarta(seleccioncarta.Carta, seleccioncarta.Seleccionada);

        }

        private void button9_Click_1(object sender, EventArgs e)
        {

            gestorjuego.Jugar();
            if (gestorjuego.Resultado() == 0)
            {
                MostrarCartas1();
            }
            else
            {
                jugadores = gestorjuego.MuestraGanador();
                if (jugadores.Count == 1)
                {
                    jugadores = gestorjuego.MuestraGanador();
                    //ganador
                    MessageBox.Show(
                     "Gano el jugador '" + jugadores[0].Usuario + "'", "Con el total de '" + jugadores[0].Puntos.Total.ToString() + "'", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Controls.Clear();
                }
                else
                {
                    label7.Text = "Empate:";
                    this.Controls.Add(listBox1);
                    listBox1.Items.Clear();
                    foreach (BE.Jugador j in jugadores)
                    {
                        listBox1.Items.Add(j.Usuario);
                    }
                    label6.Text = "Puntos:";
                    label8.Text = jugadores[0].Puntos.Total.ToString();
                }

            }

        }
        private void button6_Click(object sender, EventArgs e)
        {
            CargarAutomatico();
            ActualizarLista();
        }
        private void ValidarUsuario()
        {
            string usuario;
            string password;
            jugador.ID = 0;
            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                MessageBox.Show(
                 "El nombre esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                usuario = textBox1.Text;
                if (string.IsNullOrWhiteSpace(textBox2.Text))
                {
                    MessageBox.Show(
                     "El password esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    password = textBox2.Text;
                    jugador = gestorjugadores.Autentificar(usuario, password);
                    if (jugador.ID == 0)
                    {
                        MessageBox.Show(
                          "Usuario Incorrecto ", "Usuario cargado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        limpiar();
                    }
                    else
                    {
                        if (listBox1.Items.Contains(jugador.Usuario) == true)
                        {
                            MessageBox.Show(
                         "Usuario Incorrecto ", "El Usuario ya esta logeado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            limpiar();
                        }
                        else
                        {
                            MessageBox.Show(
                        "Usuario Correcto ", "Usuario cargado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ;
                            jugadores.Add(jugador);
                            ActualizarLista();
                            limpiar();
                        }
                    }
                }
            }
        }
        private void GenerarUsuario()
        {
            string usuario;
            string password;
            if (string.IsNullOrWhiteSpace(textBox1.Text))
            {
                MessageBox.Show(
                 "El nombre esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                usuario = textBox1.Text;
                if (string.IsNullOrWhiteSpace(textBox2.Text))
                {
                    MessageBox.Show(
                     "El password esta en blanco, favor de verificar ", "Error carga de datos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    password = textBox2.Text;
                    gestorjugadores.Insertar(usuario, password);
                    MessageBox.Show(
                     "Se genero el nuevo usuario ", "Usuario cargado", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    limpiar();
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            jugadores.Clear();
            jugadores = gestorjuego.Abandonar(jugador);
            jugadores = gestorjugadores.JugadorGanador(jugadores);
            Verresultado();
        }
        private void Verresultado()
        {
            this.Controls.Clear();
            //this.Controls.Add(button10);
            this.Controls.Add(label6);
            this.Controls.Add(label7);
            this.Controls.Add(label8);
            this.Controls.Add(label9);
            this.Controls.Add(label10);
            this.Controls.Add(label11);
            this.Controls.Add(label12);
            this.Controls.Add(label13);
            if (jugadores.Count == 1)
            {
                this.Controls.Add(dataGridView1);
                dataGridView1.DataSource = jugadores;
                label6.Text = "Ganador:";
                label8.Text = jugadores[0].Usuario;
                label7.Text = "Puntos:";
                label9.Text = jugadores[0].Puntos.Total.ToString();
            }
            else
            {
                label7.Text = "Empate:";
                this.Controls.Add(dataGridView1);
                listBox1.Items.Clear();
                dataGridView1.DataSource = jugadores;
                label6.Text = "Puntos:";
                label8.Text = jugadores[0].Puntos.Total.ToString();

            }
        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            
            if (listBox1.SelectedIndex > -1)
            {
                dataGridView1.Visible = true;
                dataGridView1.DataSource= gestorjugadores.Detalle(jugadores[index]);
            }

        }

        private void Inicio_Load(object sender, EventArgs e)
        {

        }

        private void jugadorBindingSource_CurrentChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
