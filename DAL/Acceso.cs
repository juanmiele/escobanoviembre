﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    internal class Acceso
    {
        private SqlTransaction tx;
        private SqlConnection cn;

        public void Abrir()
        {
            cn = new SqlConnection("Data Source=.\\; Initial Catalog=Escoba; Integrated Security=SSPI");
            cn.Open();
        }

        public void Cerrar()
        {
            cn.Close();
            cn = null;
            GC.Collect();
        }

        public void iniciarTransaccion()
        {
            tx = cn.BeginTransaction();
        }


        private SqlCommand CrearComando(string Sql, List<IDbDataParameter> parametros = null, CommandType tipo = CommandType.StoredProcedure)
        {
            SqlCommand comando = new SqlCommand(Sql, cn);
            comando.CommandType = tipo;
            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }
            return comando;
        }

        public int Escribir(string Sql, List<IDbDataParameter> parametros = null)
        {
            SqlCommand cmd = CrearComando(Sql, parametros);
            int resultado;
            try
            {
                resultado = cmd.ExecuteNonQuery();
            }
            catch
            {
                resultado = -1;
            }

            return resultado;
        }

        public DataTable Leer(string Sql, List<IDbDataParameter> parametros = null)
        {
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.SelectCommand = CrearComando(Sql, parametros);
            DataTable tabla = new DataTable();
            ad.Fill(tabla);

            return tabla;
        }
        //public DataTable LeerRegistro(string Sql, List<IDbDataParameter> parametros = null)
        //{
        //    SqlDataAdapter ad = new SqlDataAdapter();
        //    ad.SelectCommand = CrearComando(Sql, parametros);
        //    DataTable tabla = new DataTable();
        //    ad.Fill(tabla);
        //    DataRow registro = new DataRow(tabla);

        //    registro = tabla.Rows[0];

        //    return registro;
        //}
        public int Validar(string Sql, List<IDbDataParameter> parametros = null)
        {
            int resultado;
            SqlDataAdapter ad = new SqlDataAdapter();
            ad.SelectCommand = CrearComando(Sql, parametros);
            try
            {
                resultado = ad.SelectCommand.ExecuteNonQuery();
            }
            catch
            {
                resultado = -1;
            }
            resultado = int.Parse(ad.SelectCommand.ExecuteScalar().ToString());
            return resultado;

        }

        public IDbDataParameter CrearParametro(string nom, string valor)
        {
            SqlParameter par = new SqlParameter(nom, valor);
            par.DbType = DbType.String;
            return par;
        }

        public IDbDataParameter CrearParametro(string nom, int valor)
        {
            SqlParameter par = new SqlParameter(nom, valor);
            par.DbType = DbType.Int32;
            return par;
        }
    }
}
