﻿using BE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class MP_Bitacora
    {
        private Acceso acceso = new Acceso();

        public void GrabarBitacoraLogin(BE.Jugador jugador)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Usuario", jugador.ID.ToString()));
            parameters.Add(acceso.CrearParametro("@Partido", jugador.Bitacora.Partido.ToString()));
            parameters.Add(acceso.CrearParametro("@Login", jugador.Bitacora.Login.ToString("MM/dd/yyyy HH:mm:ss")));
            parameters.Add(acceso.CrearParametro("@Logout", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));
            parameters.Add(acceso.CrearParametro("@Fin", jugador.Bitacora.Fin.ToString()));

            acceso.Abrir();
            acceso.Escribir("INSERTAR_BITACORA_LOGIN", parameters);
            acceso.Cerrar();
        }
        public List<BE.Bitacora> ListarResultados()
        {
            List<BE.Bitacora> lista = new List<BE.Bitacora>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("listarbitacora");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Bitacora bitacora = new BE.Bitacora();
                bitacora.Usuario = int.Parse(registro[2].ToString());
                bitacora.Fin = int.Parse(registro[5].ToString());
                lista.Add(bitacora);
            }
            return lista;
        }
    }
}
