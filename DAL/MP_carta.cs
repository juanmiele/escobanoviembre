﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
   public  class MP_carta
    {

        //private Acceso acceso = new Acceso();



        public  List<BE.Carta> Listar()
        {
            List<BE.Carta> lista = new List<BE.Carta>();

            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("listarCartas");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Carta carta = new BE.Carta();
                carta.Id = int.Parse(registro[0].ToString());
                carta.Numero = int.Parse(registro[1].ToString());
                carta.Palo = int.Parse(registro[2].ToString());
                carta.Imagen = int.Parse(registro[3].ToString());
                carta.Ubicacion = 0;
                lista.Add(carta);
            }
            return lista;
        }




    }
}
