﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using BE;

namespace DAL
{
    public class MP_Log
    {
        private Acceso acceso = new Acceso();
        public void InsertarRegistro(BE.Log log)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@partido", log.Juego));
            parameters.Add(acceso.CrearParametro("@jugador", log.Jugador));
            parameters.Add(acceso.CrearParametro("@movimiento", log.Movimiento));
            parameters.Add(acceso.CrearParametro("@carta", log.Carta));
            parameters.Add(acceso.CrearParametro("@turno", log.Turno));
            acceso.Abrir();
            acceso.Escribir("INSERTAR_REGISTRO", parameters);
            acceso.Cerrar();
        }

        public int LeerUltimoJuego()
        {
            acceso.Abrir();
            DataTable tabla = acceso.Leer("ULTIMO_PARTIDO");
            acceso.Cerrar();
            DataRow registro = tabla.Rows[0];
            int juego = int.Parse(registro[1].ToString());
            juego++;
            return juego;
        }
        public void EscribirLog_Partido(BE.Juego juego,BE.Jugador jugador, string estado)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@partido", juego.Id));
            parameters.Add(acceso.CrearParametro("@jugador", jugador.ID));
            parameters.Add(acceso.CrearParametro("@estado", estado));
            parameters.Add(acceso.CrearParametro("@tiempo", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")));

            acceso.Abrir();
            acceso.Escribir("INSERTAR_BITACORA_PARTIDO", parameters);
            acceso.Cerrar();
        }
        public void EscribirLog_PartidoDetalle(BE.Juego juego, BE.Jugador jugador, string estado,BE.Carta carta, BE.Turno turno)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@partido", juego.Id));
            parameters.Add(acceso.CrearParametro("@jugador", jugador.ID));
            parameters.Add(acceso.CrearParametro("@estado", estado));
            parameters.Add(acceso.CrearParametro("@carta", carta.Id));
            parameters.Add(acceso.CrearParametro("@turno", turno.INDICE));

            acceso.Abrir();
            acceso.Escribir("INSERTAR_BITACORA_PARTIDODETALLE", parameters);
            acceso.Cerrar();
        }
    }
}
