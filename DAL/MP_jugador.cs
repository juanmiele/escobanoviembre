﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using BE;

namespace DAL
{
    public class MP_jugador
    {
        private Acceso acceso = new Acceso();

        public void Insertar(string nombre, string password)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Nombre", nombre));
            string passwordmd5 = Helper.EncodePassword(string.Concat(nombre, password));
            parameters.Add(acceso.CrearParametro("@Password", passwordmd5));
            acceso.Abrir();
            acceso.Escribir("USUARIO_INSERTAR", parameters);
            acceso.Cerrar();
        }
        public BE.Jugador EstadisticasP(BE.Jugador Jugador)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Id", Jugador.ID));
            acceso.Abrir();
            DataTable tabla = acceso.Leer("USUARIO_PARTIDAS", parameters);
            foreach (DataRow registro in tabla.Rows)
            {
                Jugador.Partidas_Jugadas = int.Parse(registro[0].ToString());
            }
            acceso.Cerrar();
            return Jugador;
        }
        public BE.Jugador EstadisticasV(BE.Jugador Jugador)
        {
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Id", Jugador.ID));
            acceso.Abrir();
            DataTable tabla = acceso.Leer("USUARIO_Victorias", parameters);
            acceso.Cerrar();
            foreach (DataRow registro in tabla.Rows)
            {

                Jugador.Victorias = int.Parse(registro[0].ToString());
            }

            return Jugador;
        }
        public BE.Jugador Tiempo(BE.Jugador Jugador)
        {

            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            parameters.Add(acceso.CrearParametro("@Id", Jugador.ID));
            acceso.Abrir();
            DataTable tabla = acceso.Leer("USUARIO_TIEMPO", parameters);
            acceso.Cerrar();
            foreach (DataRow registro in tabla.Rows)
            {

                Jugador.Min_juego = int.Parse(registro[0].ToString());
            }

            return Jugador;
        }
        public BE.Jugador Autenticar(string usuario, string password)
        {
            BE.Jugador jugador = new BE.Jugador();
            List<IDbDataParameter> parameters = new List<IDbDataParameter>();
            acceso.Abrir();
            parameters.Add(acceso.CrearParametro("@Nombre", usuario));
            string passwordmd5 = Helper.EncodePassword(string.Concat(usuario, password));
            parameters.Add(acceso.CrearParametro("@Password", passwordmd5));
            DataTable tabla = acceso.Leer("USUARIO_VALIDAR", parameters);
            acceso.Cerrar();
            foreach (DataRow registro in tabla.Rows)
            {
                jugador.ID = int.Parse(registro[0].ToString());
                jugador.Usuario = registro[1].ToString();
                jugador.Password = registro[2].ToString();
                BE.Bitacora bitacora = new BE.Bitacora();
                bitacora.Login = DateTime.Now;
                bitacora.Usuario = jugador.ID;
                jugador.Bitacora = bitacora;
            }
            
            return jugador;

        }
        internal class Helper
        {
            public static string EncodePassword(string originalPassword)
            {
                MD5 md5 = MD5.Create();
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(originalPassword);
                byte[] hash = md5.ComputeHash(inputBytes);
                return BitConverter.ToString(hash).Replace("-", "");
            }
        }
    }

}

