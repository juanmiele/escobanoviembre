USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[INSERTAR_BITACORA]    Script Date: 17/11/2020 23:38:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[INSERTAR_BITACORA]
           @Usuario int,
           @Estado char(10),
		   @Horario datetime
as
BEGIN 
declare @Id int


set @Id  = isnull( (SELECT MAX(Id) from Bitacora_Login) ,0) +1

insert into Bitacora_Login values (@id,@Usuario,@Estado,@Horario)



END
GO
USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[INSERTAR_BITACORA_LOGIN]    Script Date: 17/11/2020 23:38:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[INSERTAR_BITACORA_LOGIN]
           @Usuario int,
           @Partido int,
		   @Login datetime,
		   @Logout datetime,
		   @fin int

as
BEGIN 
declare @Id int


set @Id  = isnull( (SELECT MAX(Id) from Bitacora_Login) ,0) +1

insert into Bitacora_Login values (@id,@Partido,@Usuario,@Login,@Logout,@fin)



END
GO

USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[INSERTAR_REGISTRO]    Script Date: 17/11/2020 23:38:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[INSERTAR_REGISTRO]
           @partido int,
           @jugador int,
		   @movimiento nchar(10),
		   @carta int,
		   @turno int
as
BEGIN 
declare @id int


set @id  = isnull( (SELECT MAX(id) from Log) ,0) +1

insert Log values (@id,@partido,@jugador,@movimiento,@carta, @turno)



END
GO

USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[listarbitacora]    Script Date: 17/11/2020 23:38:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[listarbitacora]
as 
begin

select * from escoba.dbo.Bitacora_Login


END
GO
USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[listarCartas]    Script Date: 17/11/2020 23:38:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[listarCartas]
as 
begin

select * from escoba.dbo.Carta


END
GO

USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[ULTIMO_PARTIDO]    Script Date: 17/11/2020 23:38:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[ULTIMO_PARTIDO]

as
BEGIN 



SELECT * From Bitacora_Login where id = (select max(id) from Bitacora_Login)



END
GO

USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[ULTIMO_REGISTRO]    Script Date: 17/11/2020 23:39:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[ULTIMO_REGISTRO]

as
BEGIN 



SELECT * From Bitacora_Partidos where id = (select max(id) from Bitacora_Partidos)



END
GO
USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[USUARIO_ESTADISTICAS]    Script Date: 17/11/2020 23:39:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[USUARIO_ESTADISTICAS]
           @Id int
as
BEGIN 


SELECT count(Id)
  FROM [Escoba].[dbo].[Bitacora_Login] where Usuario=@Id and Fin =3

  SELECT count(Id)
  FROM [Escoba].[dbo].[Bitacora_Login] where Usuario=@id


END
GO
USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[USUARIO_INSERTAR]    Script Date: 17/11/2020 23:39:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[USUARIO_INSERTAR]
           @Nombre nchar(10),
           @Password nchar(40)
as
BEGIN 
declare @id int

set @id  = isnull( (SELECT MAX(id) from Usuario) ,0) +1

insert Usuario values (@id, @Nombre, @Password,0,0,0)


END
GO

USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[USUARIO_LEER]    Script Date: 17/11/2020 23:39:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[USUARIO_LEER]
           @Nombre nchar(10),
           @Password nchar(40)
as
BEGIN 


SELECT *
            FROM Usuario
            WHERE Nombre = @Nombre AND Password = @Password


END
GO

USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[USUARIO_PARTIDAS]    Script Date: 17/11/2020 23:39:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[USUARIO_PARTIDAS]
           @Id int
as
BEGIN 

  SELECT count(Id)
  FROM [Escoba].[dbo].[Bitacora_Login] where Usuario=6
END
GO
USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[USUARIO_TIEMPO]    Script Date: 17/11/2020 23:39:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[USUARIO_TIEMPO]
           @Id int
as
BEGIN 

select count (DATEDIFF(MINUTE,Login, Logout)) from [Escoba].[dbo].[Bitacora_Login] where Usuario=@Id


END
GO
USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[USUARIO_VALIDAr]    Script Date: 17/11/2020 23:39:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[USUARIO_VALIDAr]
           @Nombre nchar(10),
           @Password nchar(40)
as
BEGIN 


SELECT *
            FROM Usuario
            WHERE Nombre = @Nombre AND Password = @Password


END
GO
USE [Escoba]
GO

/****** Object:  StoredProcedure [dbo].[USUARIO_Victorias]    Script Date: 17/11/2020 23:39:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC  [dbo].[USUARIO_Victorias]
           @Id int
as
BEGIN 

SELECT count(Id)
  FROM [Escoba].[dbo].[Bitacora_Login] where Usuario=6 and Fin =3

END
GO









