USE [Escoba]
GO

/****** Object:  Table [dbo].[Usuario]    Script Date: 17/11/2020 23:28:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Usuario](
	[Id] [int] NOT NULL,
	[Nombre] [nchar](10) NULL,
	[Password] [nchar](40) NULL,
	[Victorias] [int] NULL,
	[Derrotas] [int] NULL,
	[Empates] [int] NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [Escoba]
GO

/****** Object:  Table [dbo].[Carta]    Script Date: 17/11/2020 23:28:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Carta](
	[Id] [int] NOT NULL,
	[Numero] [int] NOT NULL,
	[Palo] [int] NOT NULL,
	[Imagen] [int] NULL,
 CONSTRAINT [PK_Carta] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO

USE [Escoba]
GO

/****** Object:  Table [dbo].[Bitacora_Partidos]    Script Date: 17/11/2020 23:28:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Bitacora_Partidos](
	[Id] [int] NOT NULL,
	[Partido] [int] NULL,
	[Jugador] [int] NULL,
	[estado] [nchar](10) NULL,
	[tiempo] [datetime] NULL
) ON [PRIMARY]
GO

USE [Escoba]
GO

/****** Object:  Table [dbo].[Bitacora_Login]    Script Date: 17/11/2020 23:27:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Bitacora_Login](
	[Id] [int] NOT NULL,
	[Partido] [int] NULL,
	[Usuario] [int] NULL,
	[Login] [datetime] NULL,
	[Logout] [datetime] NULL,
	[Fin] [int] NULL,
 CONSTRAINT [PK_Bitacora] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


USE [Escoba]
GO

/****** Object:  Table [dbo].[Bitacora_DetallePartido]    Script Date: 17/11/2020 23:27:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Bitacora_DetallePartido](
	[Id] [int] NULL,
	[partido] [int] NULL,
	[jugador] [int] NULL,
	[movimiento] [nchar](10) NULL,
	[carta] [int] NULL,
	[turno] [int] NULL
) ON [PRIMARY]
GO




